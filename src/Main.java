import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Main extends Application {
    static int canvasHeight = 400;
    static int canvasWidth = 400;

    @Override
    public void start(Stage stage) throws Exception {
        //Set the stage
        AnchorPane anchorPane = new AnchorPane();
        Scene scene = new Scene(anchorPane, canvasWidth, canvasHeight);
        stage.setScene(scene);
        stage.show();
        stage.setTitle("Experiment");

        //Add canvas
        Canvas canvas = new Canvas();
        anchorPane.getChildren().add(canvas);
        canvas.setWidth(canvasWidth);
        canvas.setHeight(canvasHeight);
        //anchorPane.setStyle("-fx-background-color: cornsilk");
        //anchorPane.setOpacity(1);


        System.out.println(Math.sin(Math.PI/ 2));
        AnimationClass animationClass = new AnimationClass(canvas);



    }
}
