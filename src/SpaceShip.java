import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

public class SpaceShip implements Satellite{
    Canvas canvas;
    double radius;
    double xPos;
    double yPos;
    double angle;
    double xSpeed;
    double ySpeed;
    GraphicsContext gc;
    Image img;
    public SpaceShip(Canvas canvas, double radius, double initXPos, double initYPos, double angle){
        this.canvas = canvas;
        this.radius = radius;
        this.xPos = initXPos;
        this.yPos = initYPos;
        this.angle = angle;
    }

    @Override
    public void drawSatellite() {

    }

    @Override
    public void incrementPosition() {

    }

    @Override
    public void setAngle(double angle) {

    }

    @Override
    public void setSpeed(double xSpeed, double ySpeed) {

    }

    @Override
    public void moveSatellite() {

    }
}
