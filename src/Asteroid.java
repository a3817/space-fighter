import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

import java.io.File;

public class Asteroid implements Satellite{
     Canvas canvas;
     double radius;
     double xPos;
     double yPos;
     double angle;
     double xSpeed;
     double ySpeed;
     GraphicsContext gc;
     Image img;
     public Asteroid(Canvas canvas, double radius, double initXPos, double initYPos, double angle){
         this.canvas = canvas;
         this.radius = radius;
         this.xPos = initXPos;
         this.yPos = initYPos;
         this.angle = angle;
         gc = canvas.getGraphicsContext2D();
         xSpeed = 1;
         ySpeed = 1;
         img = new Image(new File("src/Images/asteroid.png").toURI().toString());
     }

     public void drawSatellite(){
         gc.setFill(Color.BLACK);
         gc.setGlobalAlpha(1);
         gc.drawImage(img, xPos, yPos, radius, radius);
     }

     public void incrementPosition(){
         xPos = xPos + Math.cos(angle)*xSpeed;
         yPos = yPos + Math.sin(angle)*ySpeed;
     }

     public void setAngle(double angle){
         this.angle = angle;
     }

    @Override
    public void setSpeed(double xSpeed, double ySpeed) {
        this.xSpeed = xSpeed;
        this.ySpeed = ySpeed;
    }

    public void moveSatellite(){
        incrementPosition();
        drawSatellite();
     }
}
