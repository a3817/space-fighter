import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.util.Duration;



public class AnimationClass {
    Timeline timeline;
    Canvas canvas;
    Asteroid asteroid;
    public AnimationClass(Canvas canvas){
        timeline = new Timeline();
        this.canvas = canvas;
        asteroid = new Asteroid(this.canvas, 25, 50, 50, Math.PI/4);
        asteroid.drawSatellite();
        animateTheGame();
    }

    public void animateTheGame(){
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.setAutoReverse(false);
        timeline.setRate(1);
        timeline.getKeyFrames().add(new KeyFrame(Duration.millis(25), new EventHandler<ActionEvent>() { //Set the speed here
            @Override
            public void handle(ActionEvent actionEvent) {
                GraphicsContext gc = canvas.getGraphicsContext2D();
                gc.clearRect(0,0, Main.canvasWidth, Main.canvasHeight);
                gc.setFill(Color.CORNSILK);
                gc.fillRect(0,0, Main.canvasWidth, Main.canvasHeight);
                asteroid.moveSatellite();
            }
        }));
        timeline.play();
    }


}
