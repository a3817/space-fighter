import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;

public interface Satellite {
    public void drawSatellite();
    public void incrementPosition();
    public void setAngle(double angle);
    public void setSpeed(double xSpeed, double ySpeed);
    public void moveSatellite();
}
